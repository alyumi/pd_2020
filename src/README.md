# Проект: "Контейнер Linux для соревнований CTF"
Целью этого проекта является разработать Docker-контейнер, в котором необходимо спрятать ключ. Студент должен будет воспользоваться средствами OC Linux и инструментами Arch Linux для поиска ключа. Ключ может быть спрятан, например, в картинке, в другом файле, в базе данных и проч.

Сайт-лендинг: http://pd-2020-1.std-1095.ist.mospolytech.ru/

## Участник
Учебная группа |	Имя пользователя |	ФИО
:------------- |:-----------------| :------------
191-351        | @alyumi          | Лебедев А.М. 
 
 ## Личный вклад участников


 ## Как запустить проект на личном ПК
 Необходимо скачать все файлы из репозитория и разместить их в нужной вам папке, а также иметь на ПК установленный заранее Docker engine.
 Для сборки проекта нужно перейти в папку с Dockerfile и ввести команду `docker build <Ваше название образа> .`.
 Затем ввести `docker run -d -p 5000:22 -p 777:80 <Ваше название образа>`.
 После этих действий стартовая точка будет доступна по адресу localhost:777 
